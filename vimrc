set nocompatible
filetype off

" Install plugins via Vundle

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'tpope/vim-sensible'       " Sensible default config
Plugin 'vim-ruby/vim-ruby'
Plugin 'tpope/vim-bundler'
"Plugin 'tpope/vim-rails'
"Plugin 'tpope/vim-haml'
Plugin 'pangloss/vim-javascript'
Plugin 'lepture/vim-jinja'
Plugin 'raimon49/requirements.txt.vim'
call vundle#end()            " required

filetype plugin indent on


set clipboard=unnamedplus " system clipboard

set tabstop=4		  " spaces to display when reading a tab character
set softtabstop=4	" spaces to insert/remove on tab/backspace
set shiftwidth=4
set expandtab		  " tabs are spaces

autocmd FileType ruby setlocal tabstop=2 shiftwidth=2 softtabstop=2

set number		    " show line numbers
set showcmd		    " show command in bottom bar
set noswapfile
set autochdir     " set vim working dir to that of current file

set history=800		" keep n lines of command history

set showcmd		    " display incomplete commands
set wildmode=longest,list,full
set wildmenu		  " display completion matches in a status line

set mouse=a       " enable visual mode on mouse selection


" Do not recognize octal numbers for Ctrl-A and Ctrl-X, most users find it
" confusing.
set nrformats-=octal

" For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries.
if has('win32')
  set guioptions-=t
endif

" Don't use Ex mode, use Q for formatting.
" Revert with ":unmap Q".
map Q gq


" Only do this part when compiled with support for autocommands.
if has("autocmd")
  " Put these in an autocmd group, so that you can revert them with:
  " ":augroup vimStartup | au! | augroup END"
  augroup vimStartup
    au!

    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid, when inside an event handler
    " (happens when dropping a file on gvim) and for a commit message (it's
    " likely a different one than last time).
    autocmd BufReadPost *
      \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
      \ |   exe "normal! g`\""
      \ | endif

  augroup END

endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
" Revert with: ":delcommand DiffOrig".
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

if has('langmap') && exists('+langremap')
  " Prevent that the langmap option applies to characters that result from a
  " mapping.  If set (default), this may break plugins (but it's backward
  " compatible).
  set nolangremap
endif

" END default vimrc

" transparent background! must be set near last to work
hi Normal ctermbg=none
hi NonText ctermbg=none
hi LineNr ctermbg=none
